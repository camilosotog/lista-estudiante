/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listaestudiantes.pojo;

import java.io.Serializable;

/**
 *
 * @author cloaiza
 */
public class Estudiante implements Serializable {
    
    private String nombre;
    private String edad;

    public Estudiante(String nombre, String edad) {
        this.nombre = nombre;
        this.edad = edad;
    }

    
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    @Override
    public String toString() {
        return "Estudiante{" + "nombre=" + nombre +
                ", edad=" + edad + '}';
    }
    
    
    
}
