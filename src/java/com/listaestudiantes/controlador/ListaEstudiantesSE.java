/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listaestudiantes.controlador;

import com.listaestudiantes.pojo.Estudiante;
import com.listaestudiantes.pojo.Nodo;
import java.io.Serializable;

/**
 *
 * @author cloaiza
 */
public class ListaEstudiantesSE implements Serializable{
    private Nodo cabeza;

    public ListaEstudiantesSE() {
    }

    public Nodo getCabeza() {
        return cabeza;
    }

    public String adicionarNodo(Estudiante info)
    {
        //Proxima Clase Disparar Excepciones
        if(this.cabeza==null)
        {
            cabeza = new Nodo(info);
            
        }
        else
        {
            Nodo temp= cabeza;
            while(temp.getSiguiente()!=null)
            {
                temp=temp.getSiguiente();
            }
             //Parado en el último  
             temp.setSiguiente(new Nodo(info));
        }   
        return "Adicionado con éxito";
        
    }
    
    public String listarNodos()
    {
        String listado="";
        if(cabeza==null)
        {
            return "La lista está vacía";
        }
        else
        {
            Nodo temp= cabeza;
            while(temp !=null)
            {
                listado += temp.getDato();
                temp= temp.getSiguiente();
            }     
            return listado;
        }    
        
    }        
    
    
}
