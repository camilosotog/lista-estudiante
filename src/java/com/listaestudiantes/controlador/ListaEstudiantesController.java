/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listaestudiantes.controlador;

import com.listaestudiantes.pojo.Estudiante;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;

/**
 *
 * @author cloaiza
 */
@Named(value = "listaEstudiantesController")
@SessionScoped
public class ListaEstudiantesController implements Serializable {
    private ListaEstudiantesSE lista= new ListaEstudiantesSE();

    private String listado;

    public String getListado() {
        return listado;
    }

    public void setListado(String listado) {
        this.listado = listado;
    }
    
    
    
    public ListaEstudiantesSE getLista() {
        return lista;
    }

    public void setLista(ListaEstudiantesSE lista) {
        this.lista = lista;
    }
    
        /**
     * Creates a new instance of ListaEstudiantesController
     */
    public ListaEstudiantesController() {
        adicionarEstudiante("Mauricio López", "20");
        
        adicionarEstudiante("Junior Celis", "21");
        mostrarListado();
        
    }
    
    public void mostrarListado()
    {
        listado= lista.listarNodos();
    }
    
    public void adicionarEstudiante(String nombre, String edad)
    {
        Estudiante estu= new Estudiante(nombre, edad);
        
        lista.adicionarNodo(estu);
        
    }        
    
}
